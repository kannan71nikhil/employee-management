/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.modal.ui;

/**
 *
 * @author student
 */
public class EmployeeTypeDetailUiModel {
         
   private  Integer id;    
     private  String employeeType;
       private  String salary;
     private  String name;
       private  String age;
       private String address;
       private String email;

    public EmployeeTypeDetailUiModel(Integer id, String employeeType, String salary, String name, String age, String address, String email) {
        this.id = id;
        this.employeeType = employeeType;
        this.salary = salary;
        this.name = name;
        this.age = age;
        this.address = address;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
       

    
}
