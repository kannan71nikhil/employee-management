/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.service;

import in.ac.gpckasaragod.employee.management.model.ui.data.EmployeeDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface EmployeeDetailsService {

    public String saveEmployeeDetails(String name, String age, String address, String email, Integer employeeTypeId);

    public EmployeeDetails readEmployeeDetails(Integer id);

    public List<EmployeeDetails> getAllEmployeeDetails();

    public String updateEmployeeDetails(Integer id, String name, String age, String address, String email, Integer employeeTypeId);

    public String deleteEmployeeDetails(Integer id);

}
