/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.service.impl;

import in.ac.gpckasaragod.employee.management.service.EmployeeTypeDetailsService;
import in.ac.gpckasaragod.employee.management.model.ui.data.EmployeeTypeDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this
 readEmployeeTypeDetails
/**
 *
 * @author student
 */
 public class EmployeeTypeDetailsServiceImpl extends ConnectionServiceImpl implements EmployeeTypeDetailsService { 
      
    
   @Override
    public EmployeeTypeDetails readEmployeeTypeDetails (Integer id) {
               EmployeeTypeDetails  employeeTypeDetails  = null;
                       try {
                            Connection connection = getConnection();
                            Statement statement = connection.createStatement();
                            String query = "SELECT * FROM EMPLOYEE_TYPE_DETAILS WHERE ID="+id;
                            ResultSet resultSet = statement.executeQuery(query);                       

                           
                         while(resultSet.next()){
                                int Id =resultSet.getInt("ID");
                               
                                String employeetype = resultSet.getString("EMPLOYEETYPE");
                                Double salary = resultSet.getDouble("SALARY");
                                employeeTypeDetails = new EmployeeTypeDetails(Id,employeetype,salary);
                                     
                               
                         }
                          return employeeTypeDetails;
                       }catch(SQLException ex) {
                            Logger.getLogger(EmployeeTypeDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
                       }
                    return employeeTypeDetails;
    }
 
      
    @Override
          public List<EmployeeTypeDetails>getAllEmployeeTypeDetailses(){
              List<EmployeeTypeDetails> EmployeeTypeDetailses = new ArrayList<>();
              try{
                  Connection connection = getConnection();
                  Statement statement = connection.createStatement();
                  String query = "SELECT * FROM EMPLOYEE_TYPE_DETAILS";
                  ResultSet resultSet = statement.executeQuery(query);
                  
                  while(resultSet.next()){
                  int id = resultSet.getInt("ID");
                  String employeeType = resultSet.getString("EMPLOYEETYPE");
                  Double salary = resultSet.getDouble("SALARY");
                  EmployeeTypeDetails employeeTypeDetails = new EmployeeTypeDetails(id,employeeType,salary);
                  EmployeeTypeDetailses.add(employeeTypeDetails);
              }
              }catch (SQLException ex){
                       Logger.getLogger(EmployeeTypeDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
                      
                      }
                        return EmployeeTypeDetailses;
             
                  
              } 
                       
    
    @Override
              public String deleteEmployeeTypeDetails(Integer id) {
                  try {
                       Connection connection = getConnection();
                       String query = "DELETE FROM EMPLOYEE_TYPE_DETAILS WHERE ID =?";
                       PreparedStatement statement = connection.prepareStatement(query);
                       statement.setInt(1,id);
                       int delete = statement.executeUpdate();
                       if(delete !=1)
                           return "Delete failed";
                       else 
                           return "Deleted successfully";
                  }catch(SQLException ex) {
                      ex.printStackTrace();
                      return "Delete failed";
                  }
                  }

    @Override
    public String saveEmployeeTypeDetails(String employeeType, String salary) {
         try {
            Connection connection = getConnection();
            Statement  statement = connection.createStatement();
                 String query = "INSERT INTO EMPLOYEE_TYPE_DETAILS (EMPLOYEETYPE,SALARY) VALUES "+"('"+employeeType+"','"+salary+"')";
                 System.err.println("Query:"+query);
                 int status = statement.executeUpdate(query);
                 if(status != 1){
                     return "Save failed";
                 }else{
                     return "Saved successfully";
                 }
                 
        } catch (SQLException ex) {
                                    
              Logger.getLogger(EmployeeTypeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
    }

   @Override
   public String updateEmployeeTypeDetails(  int id, String employeeType, String salary ){
        try{
                        Connection connection = getConnection();
                        Statement statement = connection.createStatement();                       
                        String query = "UPDATE EMPLOYEE_TYPE_DETAILS SET EMPLOYEETYPE='"+employeeType+"',SALARY='"+salary+"' WHERE ID="+id+"";
                        System.out.print(query);
                        int update = statement.executeUpdate(query);
                        if(update !=1)
                            return "Updated failed";
                        else 
                            return "Upadated sucessfully";
                    }catch(SQLException ex){
                        return "Update failed";
                    }
                    
       
    }

    public String updateEmployeeTypeDetails(String employeeType, String salary) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
              }
          
 

                
                                        

 
                       
                            
                       
    

     

         
                 
    
    

