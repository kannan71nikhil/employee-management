/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.service;

import in.ac.gpckasaragod.employee.management.model.ui.data.EmployeeTypeDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface EmployeeTypeDetailsService {
    public String saveEmployeeTypeDetails(String employeeType,String salary);    
    public EmployeeTypeDetails readEmployeeTypeDetails (Integer id);
    public List<EmployeeTypeDetails>getAllEmployeeTypeDetailses();
    
     public String updateEmployeeTypeDetails( int id,String employeeType,String salary);
   public String deleteEmployeeTypeDetails(Integer id);
    
}

