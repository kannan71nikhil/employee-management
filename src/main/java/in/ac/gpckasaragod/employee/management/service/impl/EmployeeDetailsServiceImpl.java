/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.service.impl;

import in.ac.gpckasaragod.employee.management.model.ui.data.EmployeeDetails;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.employee.management.service.EmployeeDetailsService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author student
 */
public class EmployeeDetailsServiceImpl extends ConnectionServiceImpl implements EmployeeDetailsService {

    /*public String saveEmployeeDetails(Integer employeeTypeId,String name,Integer age,String address,String email) {
         try {
            Connection connection = getConnection();
            Statement  statement = connection.createStatement();
                 String query = "INSERT INTO  EMPLOYEE_DETAILS(NAME,AGE,ADDRESS,EMAIL,EMPLOYEETYPE_ID ) VALUES "
                         +"('"+name+ "','"+age+"','"+address+"','"+email+"',"+employeeTypeId+")";
                 System.err.println("Query:"+query);
                 int status = statement.executeUpdate(query);
                 if(status != 1){
                     return "Save failed";
                 }else{
                     return "Saved successfully";
                 }
                 
        } catch (SQLException ex) {
                                    
              Logger.getLogger(EmployeeTypeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
    }*/
    @Override
    public EmployeeDetails readEmployeeDetails(Integer id) {
        EmployeeDetails employeeDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM EMPLOYEE_DETAILS WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int Id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Integer age = resultSet.getInt("AGE");
                String address = resultSet.getString("ADDRESS");
                String email = resultSet.getString("EMAIL");
                int employeeTypeId = resultSet.getInt("EMPLOYEETYPE_ID");
                employeeDetails = new EmployeeDetails(Id, name, age, address, email, employeeTypeId);

            }
            return employeeDetails;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return employeeDetails;
    }

    @Override
    public List<EmployeeDetails> getAllEmployeeDetails() {
        List<EmployeeDetails> Employees = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM EMPLOYEE_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Integer age = resultSet.getInt("AGE");
                String address = resultSet.getString("ADDRESS");
                String email = resultSet.getString("EMAIL");
                Integer employeeTypeId = resultSet.getInt("EMPLOYEETYPE_ID");

                EmployeeDetails employeeDetails = new EmployeeDetails(id, name, age, address, email, employeeTypeId);
                Employees.add(employeeDetails);

            }

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Employees;
    }

    @Override
    public String updateEmployeeDetails(Integer id, String name, String age, String address, String email, Integer employeeTypeId) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();

            String query = "UPDATE EMPLOYEE_DETAILS SET NAME='" + name + "',AGE='" + age + "' ,ADDRESS='" + address + "' ,EMAIL='" + email + "' ,EMPLOYEETYPE_ID= " + employeeTypeId + " WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Updated failed";
            } else {
                return "Updated sucessfully";
            }
        } catch (SQLException ex) {
            return "Update failed";
        }
    }

    @Override
    public String deleteEmployeeDetails(Integer id) {
        try {

            Connection connection = getConnection();
            String query = "DELETE FROM EMPLOYEE_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Deleted successfully";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return "Delete failed";
        }

    }

    @Override
    public String saveEmployeeDetails(String name, String age, String address, String email, Integer employeeTypeId) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO  EMPLOYEE_DETAILS(NAME,AGE,ADDRESS,EMAIL,EMPLOYEETYPE_ID ) VALUES "
                    + "('" + name + "','" + age + "','" + address + "','" + email + "'," + employeeTypeId + ")";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved successfully";
            }

        } catch (SQLException ex) {

            Logger.getLogger(EmployeeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
    }

}
