/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.management.model.ui.data;

/**
 *
 * @author student
 */
 public class EmployeeTypeDetails {
      
   private  Integer id;    
     private  String employeeType;
       private  Double salary;
 
    

    public EmployeeTypeDetails(Integer id, String employeeType, Double salary) {
        this.id = id;   
        this.employeeType = employeeType;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return employeeType +"-";
    }
 }
  
    
